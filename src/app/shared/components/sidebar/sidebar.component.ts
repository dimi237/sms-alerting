import { Component, OnInit } from '@angular/core';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'abw-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

  constructor(private sidebarService: NbSidebarService) { }
  items: NbMenuItem[] = [
    {
      title: 'Accueil',
      icon: 'home-outline',
    },
    {
      title: 'Templates',
      icon: 'layers-outline',
    },
    {
      title: 'Activités',
      icon: { icon: 'options-outline', pack: 'eva', status: 'primary' },
    },
    {
      title: 'Messages',
      icon: 'email-outline',
    },
    {
      title: 'Historiques',
      icon: 'list-outline',
    },
    {
      title: 'Reporting',
      icon: 'trending-up-outline',
    },
    {
      title: 'Paramètres',
      icon: 'settings-outline',
    },
  ];


  ngOnInit(): void {
    this.sidebarService.toggle(true);
  }
  toggle(): void {
    this.sidebarService.toggle(true);
  }
}
