import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { LoginComponent } from '../login/login.component';
import { filter } from 'rxjs/internal/operators/filter'

@Component({
  selector: 'abw-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  currentRout: any;

  button = {
    'message': 'Se connecter',
    'function': 'open()'
  }
  canShow = false;

  constructor(
    private dialogService: NbDialogService,
    public router: Router) {
       // tslint:disable-next-line: align
  this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe(() => {
    this.canShow = this.router.url === '/welcome';

    });
  }


  // tslint:disable-next-line: typedef
  open() {
    this.currentRout = this.router.url;
    console.log(this.currentRout);
    this.dialogService.open(LoginComponent, { context: 'this is some additional data passed to dialog' });

  }
  logout(){
    this.router.navigate(['/welcome']);
  }
  ngOnInit(): void {


    if (this.currentRout !== '/welcome'){
      this.button.message = 'Se deconnecter';
      this.button.function = 'logout()';
    }
  }





 /*  show(): boolean{

  } */
}
