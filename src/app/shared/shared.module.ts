import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { NbButtonModule, NbCardModule, NbDialogModule, NbIconModule, NbInputModule, NbMenuModule, NbSidebarModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { LoginComponent } from './components/login/login.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';



@NgModule({
  declarations: [HeaderComponent, LoginComponent, SidebarComponent],
  imports: [
    CommonModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbDialogModule.forChild(),
    NbCardModule,
    NbInputModule,
    NbSidebarModule,
    NbMenuModule,
  ],
  exports: [HeaderComponent, SidebarComponent],
})
export class SharedModule { }
